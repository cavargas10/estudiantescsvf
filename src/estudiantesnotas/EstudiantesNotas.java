package estudiantesnotas;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 *
 * @author cavargas10
 */
public class EstudiantesNotas {

    //Inicio de la clase principal (main)
    public static void main(String[] args) {
        //Declaracion de Variables
        String Linea;
        //Bloque Try - Catch en cual nos controla que todos los procesos realizados en el fichero sean correctos
        try {
            //Lee el archivo ya creado que contiene las notas de los estudiantes
            Scanner inFile = new Scanner(new File("Estudiantes.csv"));
            //Santo de Linea para evitar error
            Linea = inFile.nextLine();
            Linea = inFile.nextLine();
            //Creacion de un archivo auxialiar para no perder los datos de el fichero original
            Formatter outFile = new Formatter("cavargas10BdEst_OrdenNomb.csv.csv");
            //Formato del Archivo auxiliar
            outFile.format("%s;\n%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s\n",
                    "NOTAS DE ESTUDIANTES", "NOMBRES", "FORO1", "CHAT1", "VID1", "TRA1", "PRE1", "FORO2", "CHAT2", "VID2", "TRA2", "PRE2", "FIN1",
                    "FIN2", "TOTAL", "ALERTA", "PROMOCION");
            //En este ciclo vamoos realizando todos los procesos y almacenamos los datos en el nuevo fichero
            while (inFile.hasNext()) {
                Linea = inFile.nextLine();
                System.out.println(Linea);
                //Creacion de un arreglo para ir almacenando los datos en cada posicion separado por ";"
                String[] tokens = Linea.split(";");
                outFile.format("%s;", tokens[0]);
                outFile.format("%s;", tokens[1]);
                outFile.format("%s;", tokens[2]);
                outFile.format("%s;", tokens[3]);
                outFile.format("%s;", tokens[4]);
                outFile.format("%s;", tokens[5]);
                outFile.format("%s;", tokens[6]);
                outFile.format("%s;", tokens[7]);
                outFile.format("%s;", tokens[8]);
                outFile.format("%s;", tokens[9]);
                outFile.format("%s;", tokens[10]);
                outFile.format("%s;", tokens[11]);
                outFile.format("%s;", tokens[12]);

                double primer_Bimestre = Double.parseDouble(tokens[1]) + Double.parseDouble(tokens[2]) + Double.parseDouble(tokens[3])
                        + Double.parseDouble(tokens[4]) + Double.parseDouble(tokens[5]);
                double segundo_Bimestre = Double.parseDouble(tokens[6])
                        + Double.parseDouble(tokens[7]) + Double.parseDouble(tokens[8]) + Double.parseDouble(tokens[9])
                        + Double.parseDouble(tokens[10]);
                double sumaBimestres = primer_Bimestre + segundo_Bimestre;
                outFile.format("%d;", Math.round(sumaBimestres));
                EstudiantesNotas tarea = new EstudiantesNotas();
                String a = tarea.Alerta(tokens[5], tokens[10], primer_Bimestre, segundo_Bimestre);
                outFile.format("%s;", a);
                String b = tarea.Promocion(tokens[11], tokens[12], a, primer_Bimestre, segundo_Bimestre);
                outFile.format("%s;", b);
                outFile.format("\n");
            }
            //Fin ciclo while
            //Cierre de Ficehros Luego de realizar los cambios
            inFile.close();
            outFile.close();
            //Excepcion Lanzada
        } catch (FileNotFoundException e) {
            System.err.println("ERROR FileNotFoundException");
        } catch (NumberFormatException e) {
            System.out.println("ERROR NumberFormatException");
        }
        //Fin Bloque Try Catch
    }
    //Clase Alerta en cual nos ayuda a ver que estudiante tiene que rendir supletorios y en que Bimestre

    public String Alerta(String parcial1, String parcial2, double suma1, double suma2) {
        String mensaje = "";
        double Parcial1 = Double.parseDouble(parcial1);
        double Parcial2 = Double.parseDouble(parcial2);
        int contRF1 = 0;
        int contRF2 = 0;
        int contRF12 = 0;
        int contRF1Por = 0;
        int contRF2Por = 0;
        int contRF12Por = 0;
        int contRpFaltas = 0;
        int contRpFaltasPor = 0;
        if (Parcial1 < 8 && Parcial2 < 8 && (suma1 != 0 || suma2 != 0)) {
            mensaje = "Rendir Final 1 y 2";
            contRF12 = contRF12 + 1; //Aumenta en 1 la cantidad de Rendir Final 1 y 2
        } else {
            if (Parcial1 < 8) {
                mensaje = "Rendir Final 1";
                contRF1 = contRF1 + 1; //Aumenta en 1 la cantidad de Rendir Final 1
            }
            if (Parcial2 < 8) {
                mensaje = "Rendir Final 2";
                contRF2 = contRF2 + 1; //Aumenta en 1 la cantidad de Rendir Final 2
            }

            if (Parcial1 >= 8 && Parcial2 >= 8) {
                mensaje = "";
            } else {
                if (suma1 == suma2) {
                    mensaje = "Rendir Final 2";
                    contRF2 = contRF2 + 1; //Aumenta en 1 la cantidad de Rendir Final 2
                }
                if (suma1 > suma2) {
                    mensaje = "Rendir Final 2";
                    contRF2 = contRF2 + 1; //Aumenta en 1 la cantidad de Rendir Final 2
                } else {
                    mensaje = "Rendir Final 1";
                    contRF1 = contRF1 + 1; //Aumenta en 1 la cantidad de Rendir Final 1
                }
            }
            if (Parcial1 == 0 && Parcial2 == 0 && suma1 == 0 && suma2 == 0) {
                mensaje = "Reprobado por falta de Trabajos";
                contRpFaltas = contRpFaltas + 1; //Aumenta en 1 la cantidad de Reprobados por falta de trabajos
            }
        }

        /* Porcentajes de los estudiantes que han tenido que rendir los exámenes: 
         *  Rendir Final 1, Rendir Final 2, Rendir Final 1 y 2, y finalmente el 
         *  porcentaje de estudiantes que han Reprobado falta trabajo.
         */
        contRF1Por = (contRF1Por + contRF1) * 100;
        contRF2Por = (contRF2Por + contRF2) * 100;
        contRF12Por = (contRF12Por + contRF12) * 100;
        contRpFaltasPor = (contRpFaltas + contRpFaltasPor);

        System.out.println("Porcentaje de estudiantes a rendir Final 1: " + contRF1Por);
        System.out.println("Porcentaje de estudiantes a rendir Final 2: " + contRF2Por);
        System.out.println("Porcentaje de estudiantes a rendir Final 1 y 2: " + contRF12Por);
        System.out.println("Porcentaje de estudiantes reprobado por Flata de Trabajos: " + contRpFaltasPor);
        return mensaje;
    }
    //Fin Clase Alerta

//Metodo Clase en cual nos indica al final si el estudiante Aprobo o Reprobo el curso 
    public String Promocion(String final1, String final2, String alerta, double suma1, double suma2) {
        String mensaje = "";
        double aux;
        double Final1 = Double.parseDouble(final1);
        double Final2 = Double.parseDouble(final2);
        int contApro = 0;
        int contRepro = 0;
        int contAproPor = 0;
        int contReproPor = 0;
        if (alerta == " ") {
            mensaje = "APROBADO";
        } else {
            if (Final1 > 0) {
                aux = Final1 + suma2;
                if (aux >= 28) {
                    mensaje = "APROBADO";
                    contApro = contApro + 1; //Aumenta en 1 la cantidad de Aprobados
                } else {
                    mensaje = "REBROBADO";
                    contRepro = contRepro + 1; //Aumenta en 1 la cantidad de Reprobados

                }
            }
            if (Final2 > 0) {
                aux = Final2 + suma1;
                if (aux >= 28) {
                    mensaje = "APROBADO";
                    contApro = contApro + 1; //Aumenta en 1 la cantidad de Aprobados
                } else {
                    mensaje = "REBROBADO";
                    contRepro = contRepro + 1; //Aumenta en 1 la cantidad de Reprobados
                }

            }
            if (Final1 > 0 && Final2 > 0) {
                aux = Final1 + Final2;
                if (aux >= 28) {
                    mensaje = "APROBADO";
                    contApro = contApro + 1; //Aumenta en 1 la cantidad de Aprobados
                } else {
                    mensaje = "REBROBADO";
                    contRepro = contRepro + 1; //Aumenta en 1 la cantidad de Reprobados
                }
            }
            if (Final1 == 0 && Final2 == 0) {
                aux = suma1 + suma2;
                if (aux >= 28) {
                    mensaje = "APROBADO";
                    contApro = contApro + 1; //Aumenta en 1 la cantidad de Aprobados
                } else {
                    mensaje = "REBROBADO";
                    contRepro = contRepro + 1; //Aumenta en 1 la cantidad de Reprobados
                }
            }

        }
        //Calcular Porcentaje de Aprobados y Reprobados
        contAproPor = (contAproPor + contApro) * 100;
        contReproPor = (contReproPor + contRepro) * 100;

        System.out.println("Porcentaje de Aprobados: " + contAproPor);
        System.out.println("Porcentaje de Reprobados: " + contReproPor);
        return mensaje;
    }
    //Fin Clase Promocion
}
 //Fin Clase Estudiante
